# backoffice-uikit

Al momento ci sono due modi per strutturare una cartella di componenti:

## Singolo

```
..Component/
  - component.js
  - component.css
  - index.js
```

index.js
```
export const Component = () => {
  return ()
}
```

In questo modo è importabile così:

`import { Component } from '@monorepov3/backoffice-uikit/dist/Component';`

## Gruppo

```
..GroupComponents/
  ..Component1/
    - component1.js
    - component1.css
    - index.js
  ..Component2/
    - component2.js
    - component2.css
    - index.js
  index.js
```

GroupComponents/index.js
```
export { Component1 } from "./Component1";
export { Component2 } from "./Component2";
```

GroupComponents/Component1/ndex.js
```
export const Component1 = () => {
  return ()
}
```

In questo modo è importabile così:

`import { Component1 } from '@alpha/ui-kit/dist/GroupComponents';`


### TODO/IMPROVEMENTS

- [ ] Al momento non viene utilizzato css-modules o altre librerie per gestire il css, valutiamo al bisogno
      man mano che arrivano i casi da analizzare
- [ ] Test
- [ ] Documentazione aggiuntiva da fare per i componenti se non basta quella generata in automatico