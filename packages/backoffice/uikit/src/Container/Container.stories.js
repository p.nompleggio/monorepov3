import React from "react";

import { Container } from "./index";

export default {
  title: "Example/Container",
  component: Container,
  decorators: [
    (Story) => (
      <div style={{ backgroundColor: '#f7d794' }}>
        <Story/>
      </div>
    ),
  ],
};

const Template = (args) => <Container {...args} />;

export const Default = Template.bind({});
Default.args = {
  children: <div style={{backgroundColor: '#e66767', height: '100px', width: '100px'}}></div>
};

export const Center = Template.bind({});
Center.args = {
  children: <div style={{backgroundColor: '#e66767', height: '100px', width: '100px'}}></div>,
  center: true
};

export const CenteredAnd100vh = Template.bind({});
CenteredAnd100vh.args = {
  children: <div style={{backgroundColor: '#e66767', height: '100px', width: '100px'}}></div>,
  center: true,
  fullViewportHeight: true
};