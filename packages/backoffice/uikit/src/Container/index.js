import React from "react";
import PropTypes from "prop-types";
import "./container.css";

/**
 * Container for all components
 */
export const Container = ({ children, direction, fullViewportHeight, center  }) => {

  const _direction = direction === 'column' ? "storybook-container--direction-column" : "storybook-container--direction-row";
  const _fullViewportHeight = fullViewportHeight ? "storybook-container--100vh" : "";
  const _center = center ? "storybook-container-center" : "";

  return (
    <div
      className={["storybook-container-base", _direction, _fullViewportHeight, _center].join(" ")}
    >
      {children}
    </div>
  );
};

Container.propTypes = {
  /**
   * Children to be displayed
   */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  /**
   * Direction of alignment
   */
  direction: PropTypes.oneOf(["column", "row"]),
  /**
   * Use all viewport height or just content
   */
  fullViewportHeight: PropTypes.bool,
  /**
   * Center content horizontally and vertically
   */
  center: PropTypes.bool
};

Container.defaultProps = {
  children: null,
  direction: 'column',
  fullViewportHeight: false,
  center: false
};
