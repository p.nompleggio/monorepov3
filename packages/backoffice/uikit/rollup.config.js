import fs from "fs";
import fs_extra from "fs-extra";
import path from "path";
import { babel } from "@rollup/plugin-babel";
import { nodeResolve } from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import peerDepsExternal from "rollup-plugin-peer-deps-external";
import postcss from "rollup-plugin-postcss";

fs_extra.emptyDirSync("dist");

const config = fs
  .readdirSync("src")
  .reduce((acc, dir) => {
    const stats = fs.statSync(path.join("src", dir));
    if (!stats.isFile() && fs.readdirSync(`src/${dir}`).filter((f) => f.indexOf("index.js") !== -1).length !== 0) {
      acc.push(`src/${dir}`);
    }
    return acc;
  }, [])
  .map((component) => ({
    input: path.join(component, "index.js"),
    plugins: [
      peerDepsExternal(),
      postcss({
        extensions: [".css"],
      }),
      babel({
        babelHelpers: "runtime",
      }),
      nodeResolve(),
      commonjs(),
    ],
    output: [
      {
        file: `dist/${component.split("/").pop()}.js`,
        format: "es",
      },
      {
        file: `dist/cjs/${component.split("/").pop()}.js`,
        format: "cjs",
        exports: "named",
      },
    ],
  }));

export default config;
