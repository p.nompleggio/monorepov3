const path = require("path");

module.exports = {
  resolve: {
    alias: {
      storage: path.resolve(__dirname, "src/dev/common/storage"),
      rehydrate: path.resolve(__dirname, "src/dev/common/rehydrate"),
      constants: path.resolve(__dirname, "src/dev/common/constants"),
      api: path.resolve(__dirname, "src/dev/common/api"),
    },
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
    ],
  },
};
