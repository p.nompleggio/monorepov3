import React from "react";
import { Redirect, Route } from "react-router-dom";
import { isEmpty } from "@monorepov3/common";
import { useSelector } from "react-redux";
import { selectToken } from "./authSlice";
import PropTypes from "prop-types";

export const PrivateRoute = ({ component: Component, ...rest }) => {
  const token = useSelector(selectToken);

  return (
    <Route
      {...rest}
      render={(props) => {
        if (!isEmpty(token)) {
          return <Component {...props} />;
        }

        return (
          <Redirect
            to={{
              pathname: "/login",
              /* eslint-disable react/prop-types */
              state: { from: props.location },
            }}
          />
        );
      }}
    />
  );
};

PrivateRoute.propTypes = {
  component: PropTypes.elementType,
};

PrivateRoute.defaultProps = {
  component: null,
};
