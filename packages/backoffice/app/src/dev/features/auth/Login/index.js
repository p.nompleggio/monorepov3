import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { login, selectToken } from "../authSlice";
import { useHistory } from "react-router-dom";
import { Container } from "@monorepov3/backoffice-uikit/dist/Container";
import { Button } from "@monorepov3/backoffice-uikit/dist/Buttons";

export default function Login() {
  const history = useHistory();
  const dispatch = useDispatch();
  const token = useSelector(selectToken);
  const [loginRequestStatus, setLoginRequestStatus] = useState("idle");
  const [email, setEmail] = useState("p.nompleggio@tuotempo.com");
  const [password, setPassword] = useState("Tuotempo1234");

  useEffect(() => {
    if (token !== null) {
      history.push("/");
    }
  }, []);

  return (
    <Container>
      <input
        placeholder={"email"}
        value={email}
        onChange={(e) => {
          setEmail(e.target.value);
        }}
      />
      <input
        placeholder={"password"}
        value={password}
        type="password"
        onChange={(e) => {
          setPassword(e.target.value);
        }}
      />
      <Button
        label={"Login"}
        primary={true}
        disabled={loginRequestStatus !== "idle"}
        onClick={async () => {
          try {
            setLoginRequestStatus("pending");
            await dispatch(login({ username: email, password: password }));
            history.push("/");
          } catch (err) {
            setLoginRequestStatus("idle");
            console.error("Failed to save the post: ", err);
          }
        }}
      />
    </Container>
  );
}
