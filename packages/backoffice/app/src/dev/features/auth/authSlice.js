import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { getUserFromStorage } from "rehydrate";
import { API } from "api";
import storage from "storage";
import { BACKOFFICE_STORAGE_USER } from "constants";

export const login = createAsyncThunk("auth/login", async ({ username, password }) => {
  const response = await API({
    method: "post",
    url: "json.php",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    data: {
      user: username,
      password,
    },
    params: {
      fn: "login",
      dbName: "tt_piero",
      version: 1.1,
      lang: "en",
      client: "desktop",
    },
    serializeParams: true,
    serializeData: true,
  });

  return {
    response,
  };
});

const initialState = getUserFromStorage();
const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    logout: (/* state*/) => {
      storage.clearAppStorage();
      // TODO: Come pulire al logout tutti i reducer? una soluzione potrebbe essere quella di chiamare qui dentro una funzione clean per ogni slice che inizializza initialState
      // >>>>> https://stackoverflow.com/a/61943631/678833
      return initialState;
    },
  },
  extraReducers: {
    [login.fulfilled]: (state, { payload }) => {
      const user = payload.response.return.fname;
      const token = payload.response.sessionid;
      state.token = token;
      state.user = user;
      storage.set({ user, token }, BACKOFFICE_STORAGE_USER);
    },
  },
});

export default authSlice.reducer;
export const { logout } = authSlice.actions;

export const selectToken = (state) => state.auth.token;

export const selectUser = (state) => state.auth.user;
