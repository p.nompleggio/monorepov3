import React from 'react';

export default function NotFound404() {
  return (
    <div style={{width: '100%', justifyContent: 'center', display: 'flex'}}>
      Not Found
    </div>
  );
}