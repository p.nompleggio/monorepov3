import React, { useEffect } from "react";
import { render } from "react-dom";
import { rollDice } from "@monorepov3/common";
import { Router, Switch, Route } from "react-router";
import { createBrowserHistory } from "history";
import store from "./store";
import { Provider } from "react-redux";
import { PrivateRoute } from "./features/auth/PrivateRoute";

const history = createBrowserHistory();
const Login = React.lazy(() => import(/* webpackChunkName: "login" */ "./features/auth/Login"));
const App = React.lazy(() => import(/* webpackChunkName: "login" */ "./App"));

const Main = () => {
  useEffect(() => {
    console.log(rollDice());
  }, []);

  return (
    <Provider store={store}>
      <Router history={history}>
        <React.Suspense fallback={<div></div>}>
          <Switch>
            <Route path="/login" component={Login} />
            <PrivateRoute exact path={["*"]} component={App} />
          </Switch>
        </React.Suspense>
      </Router>
    </Provider>
  );
};

render(<Main />, document.getElementById("root"));
