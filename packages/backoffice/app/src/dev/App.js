import React from "react";
import { Header } from "@monorepov3/backoffice-uikit/dist/Header";
import { Switch, Route, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { selectUser, logout } from "./features/auth/authSlice";

const NotFound = React.lazy(() => import(/* webpackChunkName: "notfound" */ "./NotFound"));
const Dashboard = React.lazy(() => import(/* webpackChunkName: "notfound" */ "../modules/Dashboard"));
const Nothing = React.lazy(() => import(/* webpackChunkName: "notfound" */ "../modules/Nothing"));

export default function App() {
  const user = useSelector(selectUser);
  const dispatch = useDispatch();
  const history = useHistory();

  return (
    <>
      <Header
        user={{ user }}
        onLogin={() => {}}
        onLogout={() => {
          dispatch(logout());
          history.push("/login");
        }}
        items={["dashboard", "nothing"]}
        onItem={(it) => {
          history.push(it);
        }}
      />
      <div>
        <React.Suspense fallback={<div></div>}>
          <Switch>
            <Route exact path="/dashboard">
              <Dashboard />
            </Route>
            <Route exact path="/nothing">
              <Nothing />
            </Route>
            <Route exact path="*" component={NotFound} />
          </Switch>
        </React.Suspense>
      </div>
    </>
  );
}
