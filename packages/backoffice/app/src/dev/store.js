import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./features/auth/authSlice";
import usersSlice from "../features/users/userSlice";

export default configureStore({
  reducer: {
    auth: authReducer,
    users: usersSlice,
  },
});
