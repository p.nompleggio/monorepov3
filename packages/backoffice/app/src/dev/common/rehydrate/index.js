import storage from "storage";
import { BACKOFFICE_STORAGE_USER } from "constants";
import { isEmpty } from "@monorepov3/common";

export const getEmptyUser = () => {
  return {
    token: null,
    user: null,
  };
};

export const getUserFromStorage = () => {
  const user = storage.get(BACKOFFICE_STORAGE_USER);

  if (!isEmpty(user)) {
    return user;
  }

  return getEmptyUser();
};
