import { HTTPRequest } from "@monorepov3/common";
import { getUserFromStorage } from "rehydrate";

export const API = (request) => {
  const user = getUserFromStorage();
  if (user.token !== null) {
    request.token = user.token;
  }
  return HTTPRequest(request);
};
