const storage = {
  /**
   * Remove an item from the used storage
   * @param  {String} key [description]
   */
  clear(key) {
    if (localStorage && localStorage.getItem(key)) {
      return localStorage.removeItem(key);
    }

    if (sessionStorage && sessionStorage.getItem(key)) {
      return sessionStorage.removeItem(key);
    }

    return null;
  },

  /**
   * Clear all app storage
   */
  clearAppStorage() {
    if (localStorage) {
      localStorage.clear();
    }

    if (sessionStorage) {
      sessionStorage.clear();
    }
  },

  /**
   * Returns data from storage
   * @param  {String} key Item to get from the storage
   * @return {String|Object}     Data from the storage
   */
  get(key) {
    if (localStorage && localStorage.getItem(key)) {
      return JSON.parse(localStorage.getItem(key)) || null;
    }

    if (sessionStorage && sessionStorage.getItem(key)) {
      return JSON.parse(sessionStorage.getItem(key)) || null;
    }

    return null;
  },

  /**
   * Set data in storage
   * @param {String|Object}  value    The data to store
   * @param {String}  key
   * @param {Boolean} isLocalStorage  Defines if we need to store in localStorage or sessionStorage
   */
  set(value, key, isLocalStorage = false) {
    const isUpdate = () => {
      if (localStorage && localStorage.getItem(key)) {
        return "local";
      }

      if (sessionStorage && sessionStorage.getItem(key)) {
        return "session";
      }

      return null;
    };

    if ((isLocalStorage || isUpdate() === "local") && localStorage) {
      return localStorage.setItem(key, JSON.stringify(value));
    }

    if (sessionStorage || isUpdate() === "session") {
      return sessionStorage.setItem(key, JSON.stringify(value));
    }

    return null;
  },
};

export default storage;
