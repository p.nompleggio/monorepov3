import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getUsers, selectUsers } from "./userSlice";
import { Container } from "@monorepov3/backoffice-uikit/dist/Container";

export default function Users() {
  const dispatch = useDispatch();
  const users = useSelector(selectUsers);

  useEffect(() => {
    dispatch(getUsers());
  }, []);

  return (
    <Container direction={"row"}>
      {users &&
        users.map((u, i) => {
          return (
            <div key={i} style={{ padding: "10px" }}>
              <img src={u.photo} />
              <div style={{ textAlign: "center" }}>{u.name}</div>
            </div>
          );
        })}
    </Container>
  );
}
