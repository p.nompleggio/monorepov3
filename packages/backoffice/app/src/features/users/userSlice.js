import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const getUsers = createAsyncThunk("users/get", async () => {
  const resp = await axios.get("https://run.mocky.io/v3/dacc9ddd-a340-42e0-8d11-4197b5553f16");

  return resp.data;
});

const initialState = { data: [] };

const userSlice = createSlice({
  name: "users",
  initialState,
  extraReducers: {
    [getUsers.fulfilled]: (state, { payload }) => {
      state.data = payload;
    },
  },
});

export default userSlice.reducer;

export const selectUsers = (state) => state.users.data;
