import React from "react";
import { render } from "react-dom";
import Dashboard from "./index";
import { configureStore } from "@reduxjs/toolkit";
import usersSlice from "../../features/users/userSlice";
import { Provider } from "react-redux";

const store = configureStore({
  reducer: {
    users: usersSlice,
  },
});

const Main = () => {
  return (
    <Provider store={store}>
      <Dashboard />
    </Provider>
  );
};

render(<Main />, document.getElementById("dashboard"));
