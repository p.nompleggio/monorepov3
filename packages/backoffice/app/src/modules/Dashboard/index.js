import React from "react";
import { Container } from "@monorepov3/backoffice-uikit/dist/Container";
import Users from "../../features/users/Users";

export default function Dashboard() {
  return (
    <Container>
      <Users />
    </Container>
  );
}
