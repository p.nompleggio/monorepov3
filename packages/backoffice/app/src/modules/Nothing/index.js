import React from "react";
import { Container } from "@monorepov3/backoffice-uikit/dist/Container";

export default function Nothing() {
  return (
    <Container>
      <div style={{ backgroundColor: "#e66767", height: "100px", width: "100px" }}></div>
    </Container>
  );
}
