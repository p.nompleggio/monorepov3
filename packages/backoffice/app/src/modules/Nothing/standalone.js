import React from "react";
import { render } from "react-dom";
import Nothing from "./index";
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";

const store = configureStore({
  reducer: {},
});

const Main = () => {
  return (
    <Provider store={store}>
      <Nothing />
    </Provider>
  );
};

render(<Main />, document.getElementById("nothing"));
