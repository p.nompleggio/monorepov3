const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const commonConfiguration = require("./webpack.config.common");

module.exports = () => {
  return {
    mode: "development",
    devtool: "source-map",
    entry: "./src/dev/index.js",
    output: {
      path: `${__dirname}/dist`,
      filename: "[name].[contenthash].js",
    },
    devServer: {
      open: false,
      historyApiFallback: true,
      proxy: [
        {
          context: ["/json.php", "/api"],
          target: "http://192.168.33.10",
          changeOrigin: true,
          secure: false,
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: "./src/dev/index.html",
      }),
      new CleanWebpackPlugin(),
    ],
    ...commonConfiguration,
  };
};
