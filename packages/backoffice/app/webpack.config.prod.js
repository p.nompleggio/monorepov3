const fs = require("fs");
const path = require("path");
const fs_extra = require("fs-extra");
const webpack = require("webpack");
const { WebpackManifestPlugin } = require("webpack-manifest-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const commonConfiguration = require("./webpack.config.common");

fs_extra.emptyDirSync("dist");

const base = `src/modules`;
const modules = fs.readdirSync(base).reduce((acc, dir) => {
  console.log(dir);
  const stats = fs.statSync(path.join(base, dir));
  if (
    !stats.isFile() &&
    fs.readdirSync(path.join(base, dir)).filter((f) => f.indexOf("standalone.js") !== -1).length !== 0
  ) {
    acc[dir.toLowerCase()] = path.join(base, dir, "standalone.js");
  }
  return acc;
}, {});

const keys = Object.keys(modules);

const build = async () => {
  const compilePromise = (entry) => {
    return new Promise((resolve, reject) => {
      const compiler = webpack({
        mode: "production",
        devtool: "source-map",
        entry: {
          [entry]: `./${modules[entry]}`,
        },
        output: {
          path: `${__dirname}/dist/${entry}`,
          filename: `[name].[contenthash].js`,
        },
        optimization: {
          splitChunks: {
            cacheGroups: {
              commons: {
                test: /[\\/]node_modules[\\/]/,
                name: "vendors",
                chunks: "all",
              },
            },
          },
        },
        plugins: [
          new WebpackManifestPlugin({
            filter: (file) => !file.path.match(/\.(map)$/),
            publicPath: "",
            // eslint-disable-next-line no-unused-vars
            sort: (a, b) => {
              // compareFunction(a, b) returns a value > than 0, sort b before a
              // compareFunction(a, b) returns a value < than 0, sort a before b
              // compareFunction(a, b) returns 0, a and b are considered equal.
              if (a.name.indexOf("vendors") !== -1) {
                return -1; // Qui è fondamentle che vendors venga sempre messo prima di tutte le altre chiavi
              }

              return 1;
            },
          }),
          new CleanWebpackPlugin({
            protectWebpackAssets: false,
            cleanAfterEveryBuildPatterns: ["*.LICENSE.txt"],
          }),
        ],
        ...commonConfiguration,
      });

      compiler.run((err, stats) => {
        // [Stats Object](#stats-object)
        // console.log("Finito"+i);
        compiler.close((closeErr) => {
          // ...
          if (err || stats.hasErrors() || closeErr) {
            return reject();
          }

          return resolve();
        });
      });
    });
  };

  for (let i = 0; i < Object.keys(modules).length; i += 1) {
    console.log(`Building ${keys[i]} ... ${i + 1}/${keys.length}`);
    await compilePromise(keys[i]);
  }

  console.log(`Builded successfully ${keys.length} modules.`);
};

build();
