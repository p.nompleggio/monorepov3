export const getRandomArbitrary = (min, max) => {
  return Math.floor((Math.random() * (max - min)) + min);
};

export const rollDice = () => {
  return `I rolled a dice: ${getRandomArbitrary(1, 6)}.`;
};

export const isEmpty = (value) => {
  if (value === "" || value === null || value === undefined) return true;

  if (typeof value === 'object') return [Object, Array].includes((value || {}).constructor) && !Object.entries((value || {})).length;

  return false;
};

export { HTTPRequest } from './http';