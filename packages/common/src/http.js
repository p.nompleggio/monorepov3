import axios from "axios";
import qs from "qs";

export const HTTPRequest = (request) => {
  const path = `http://localhost:8081/${request.url}`;

  const options = {
    headers: {},
    method: request.method,
  };

  if (request.token !== undefined && request.token !== "") {
    options.headers.Authorization = `Bearer ${request.token}`;
  }

  if (request.headers && request.headers !== null) {
    options.headers = Object.assign({}, options.headers, request.headers);
  }

  if (request.data !== undefined) {
    options.data = request.data;
  }

  if (request.params !== undefined) {
    options.params = request.params;
  }

  if (request.serializeParams === true) {
    options.paramsSerializer = (params) => {
      return qs.stringify(params);
    };
  }

  if (request.serializeData === true) {
    options.transformRequest = [
      (data) => {
        return qs.stringify(data);
      },
    ];
  }

  /*
   url: '/user',
   method: 'get',
   headers: {'X-Requested-With': 'XMLHttpRequest'},
   serializeParams: true | false > default false, serializza i parametri per quando si mettono in querystring per esempio
   params: are the URL parameters to be sent with the request (GET)
   data: parametri per 'PUT', 'POST', and 'PATCH',
   serializeData: come per i param
  */

  return axios
    .request(path, options)
    .then((response) => {
      return Promise.resolve(response.data);
    })
    .catch((error) => {
      return Promise.reject(error.response.data);
    });
};
