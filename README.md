# monorepov3

1. `yarn set version berry`
2. `yarn init -w`
3. `yarn plugin import workspace-tools`
4. `yarn workspace @monorepov3/backoffice-app add @monorepov3/common`
5. `yarn install`
6. `yarn workspaces foreach -pi run watch`
7. `yarn workspaces foreach -pt run build`
8. `yarn workspaces foreach --include @monorepov3/backoffice-app -ptR run watch` >>>> con il -R non funziona si blocca subito

# Per creare un nuovo package

1. creare la cartella del nuovo package all'interno di packages all'interno del giusto percorso
2. andare con il terminale nel percorso della nuova cartella
3. eseguire yarn init (aggiungere versione e quello che manca vedere gli altri package)
4. tornare nella root del progetto
5. eseguire yarn install (che effettuare i link del progetto)
  
# Come pushare 

Utilizzare `yarn commit` oppure `yarn commit:all`.

### TOWER + GIT HOOKS

1. Create this file: ~/Library/Application Support/com.fournova.Tower3/environment.plist

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
    <dict>
        <key>PATH</key>
        <string>/Users/piero/.yarn/bin:/Users/piero/.config/yarn/global/node_modules/.bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin</string>
    </dict>
</plist>
```

### Sviluppo
`yarn workspaces foreach -pi run watch`

### Sviluppo Backoffice + Storybook
`concurrently "yarn workspaces foreach -pi run watch" "yarn workspace @monorepov3/backoffice-uikit storybook"`

### TODO/IMPROVEMENTS

- [ ] Browser compatibility cerchiamo di fissare le versioni con le percentuali per evitare di transpilare sempre più del necessario
- [ ] Nel prepare-branch invece di sporcare il package.json di root si potrebbe creare un file nella root del progetto scrivendoci sempre dentro il nome
      della versione di prerelease per andarla poi a leggere
- [ ] Cachare node_modules per velocizzare pipeline
- [ ] Bloccare commit diretti su master, si può pushare solo tramite merge request
- [ ] Sentry
- [ ] test E2E
- [ ] axios - mock-adapter 

