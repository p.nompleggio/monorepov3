const chalk = require("chalk");

const argv = require("yargs-parser")(process.argv.slice(2));
console.log(argv);

if (!("command" in argv)) {
  console.log(chalk.underline.red("command argument not found."));
  return process.exit();
}

if (!("gitlabci" in argv)) {
  console.log(chalk.underline.red("gitlabci argument not found."));
  return process.exit();
}

const { command, gitlabci } = argv;
const CICore = require("./core");
let core = new CICore(JSON.parse(gitlabci));

if (command === "prepare-branch") {
  core.prepareBranch();
} else if (command === "dev-release") {
  core.env = "development";
  core.devRelease();
} else if (command === "dev-rebuild") {
  core.env = "development";
  core.rebuildAllBranchHistory();
} else if (command === "prod-release") {
  core.env = "production";
  core.prodRelease();
} else if (command === "prod-rebuild") {
  core.env = "production";
  core.prodRebuildAllPackages();
}
