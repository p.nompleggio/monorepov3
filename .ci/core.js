/**
 * Questo script partirà quando verrà fatto un commit con [ci release]
 */

const { execSync } = require("child_process");
const chalk = require("chalk");
const path = require("path");
const cwd = process.cwd();
const fs = require("fs");
const emoji = require("node-emoji");

const commitLogToJSON = `--pretty=format:'{%n  "hash": "%H",%n  "author": "%aN <%aE>",%n  "date": "%at",%n  "message": "%f"%n},' \
$@ | \
perl -pe 'BEGIN{print "["}; END{print "]\n"}' | \
perl -pe 's/},]/}]/'`;

const execOptions = { encoding: "utf8" };

const STRATEGY_MAJOR = "major";
const STRATEGY_MINOR = "minor";
const STRATEGY_PATCH = "patch";
const STRATEGY_PRERELEASE = "prerelease";
const STRATEGY_PREMAJOR = "premajor";

class CICore {
  constructor({ branch, access_token, project_id, repo_url, user_key }, env) {
    this.branch = branch;
    this.access_token = access_token;
    this.project_id = project_id;
    this.user_key = user_key;
    this.repo_url = repo_url;
    this.env = env;
  }

  /**
   *
   * @param {string} branchName
   * @returns array > [fromHash, toHash] first and last hash commit ever on this branch,
   *          this two can be the same if there is only one commit in the branchor an empty array in case of error
   */
  getFirstAndLastHashOnBranch() {
    try {
      // Prendo il primo commit fatto in automatico quando si è creato il branch
      const [initialCommit] = JSON.parse(
        execSync(`git log --grep="Initial commit ${this.branch}" ${commitLogToJSON}`, execOptions)
      );
      // Prendo l'ultimo commit del branch
      const [lastCommit] = JSON.parse(execSync(`git log -n 1 ${commitLogToJSON}`, execOptions));

      return [initialCommit.hash, lastCommit.hash];
    } catch (error) {
      return [];
    }
  }

  /**
   *
   * @returns array, con due hash commit presi dal primo e dall'ultimo in base al comando di CI.
   *          [ci release] - ritorna il primo e l'ultimo commit hash commit dal penultimo comando di release + 1 all'ultimo
   *          con il comando di release che ha scatenato la pipeline. AL MOMENTO QUESTO è L'UNICO COMANDO.
   */
  filterCommitsBasedOnLastCommand(command = "ci-release", fromHash, toHash) {
    try {
      // Se il primo è l'ultimo sono uguali allora vuol dire che non ci sono stati altri commit e
      // non possiamo effettuare nessuna operazione di versionamento
      if (fromHash === toHash) {
        return [];
      }

      // Recupero tutti i commit, --ancestry-path mi rimuove i commit mergiati da un'altro branch (es. master)
      // rimangono però i commit dei merge, ma quelli non hanno file modificati.
      // Questo log non prende il primo commit non è incluso quindi Initial commit [branch]
      const commits = JSON.parse(
        execSync(`git log --ancestry-path ${fromHash}..${toHash} ${commitLogToJSON}`, execOptions)
      );

      // Ordiniamo i commit da quello più recente
      commits.sort(function (x, y) {
        return y.date - x.date;
      });

      if (commits.length === 0) {
        console.log(chalk.underline.red(`No commits found.`));
        return [];
      }

      if (command === "ci-release") {
        // Ora controlliamo se esiste un commit di release tra tutti i commit, generalmente questo dovrebbe essere
        // il caso ideale e quindi dovrebbe sempre essere l'ultimo commit (il primo visto che sono ordinati dal più recente) perchè questo script al momento è stato
        // pensato e viene lanciato ogni volta che viene fatto un commit con [ci release] (c'è ci-release perchè per evitare codifiche strane vengono)
        // messi i trattini negli spazi
        let existAlreadyRelease = commits.find((c) => c.message.includes("ci-release"));
        let existAlreadyReleaseIndex = commits.findIndex((c) => c.message.includes("ci-release"));

        let hash1ToCompare = fromHash;
        const lastHashToCompare = existAlreadyRelease.hash;
        // Esiste già una release quindi prendiamo tutti i commit da quello all'ultimo
        if (existAlreadyRelease !== undefined) {
          // Se il commit di release è l'ultimo commit allora vuol dire che è quello che abbiamo fatto
          // quindi dobbiamo vedere se prima di questo esiste un commit di release per prendere tutti quelli nel mezzo
          if (existAlreadyRelease.hash === toHash) {
            // Rimuovo l'ultimo ci release e vediamo se ce ne sono prima di questo, faccio prima una copia perchè
            // splice modifica l'array stesso
            const copiedCommits = [...commits];
            copiedCommits.splice(existAlreadyReleaseIndex, 1);
            // Ora controllo se esiste un'altro ci release
            let secondLastRelease = copiedCommits.find((c) => c.message.includes("ci-release"));
            const secondLastReleaseIndex = copiedCommits.findIndex((c) => c.message.includes("ci-release"));

            // In fase di sviluppo esiste anche un'altro comando che è [ci dev-rebuild] quindi quando si guarda il penultimo [ci-release]
            // dobbiamo anche guardare [ci dev-rebuild] perchè se è l'ulitimo commit è quello da prendere, perchè da li è stato fatto un
            // rebuild di tutti i packages modificati in questo branch di sviluppo.
            const existRebuildAll = copiedCommits.find((c) => c.message.includes("ci-dev-rebuild"));
            const existRebuildAllIndex = copiedCommits.findIndex((c) => c.message.includes("ci-dev-rebuild"));

            // Visto che ho già messo secondLastRelease nella variabile che poi viene letta dopo, i controlli sotto li faccio solo
            // rispetto alla rebuild-all che se esiste ed è più recente (o se l'altra non esiste) prende il posto dell'altra.

            // Esistono entrambe
            if (secondLastReleaseIndex !== -1 && existRebuildAllIndex !== -1) {
              // guardo la più recente (quindi quella che ha l'indice minore)
              if (existRebuildAllIndex < secondLastReleaseIndex) {
                secondLastRelease = existRebuildAll;
              }
              // se non esiste la release normale ma esiste il rebuild all allora prendo quella
            } else if (secondLastReleaseIndex === -1 && existRebuildAllIndex !== -1) {
              secondLastRelease = existRebuildAll;
            }

            if (secondLastRelease !== undefined) {
              // Esiste posso partire dal commit successivo
              const _hashIndex = commits.findIndex((c) => c.hash === secondLastRelease.hash);
              // il -1 è perchè è stato ordinato dal più recente, quindi il più recente ha indice 0, quindi dobbiamo andare indietro
              hash1ToCompare = commits[_hashIndex - 1].hash;
            }
          }

          return [hash1ToCompare, lastHashToCompare];
        }
      }

      return [fromHash, toHash];
    } catch (error) {
      console.log(`error: ${error}`);
      return [];
    }
  }

  /**
   *
   * @returns Ritorna tutti i packages che fanno parte del workspace
   */
  getAllPackages() {
    const packagesResult = execSync(`yarn workspaces list --json`, execOptions);
    const allPackages = packagesResult
      .trim()
      .split("\n")
      .map((p) => JSON.parse(p));

    return allPackages.filter((p) => p.name !== "monorepov3");
  }

  /**
   *
   * @param {string} fromHash hash iniziale
   * @param {string} toHash hash finale
   * @returns ritorna un array di pacchetti cambiati all'interno del workspace, torna array vuoto
   *          se non sono stati rirovati cambiamenti
   */
  getChangedPackages(fromHash, toHash) {
    const changed = [];

    if (!fromHash) {
      console.log(chalk.underline.red(`You have not provided even initial hash commit.`));
      return changed;
    }

    const allPackages = this.getAllPackages();

    // Ritiroan tutti i commits nel range passato (compresi)
    const allCommitsID = execSync(`git rev-list --ancestry-path ${fromHash}^..${toHash}`, execOptions)
      .trim()
      .split("\n");

    const changedFiles = new Set();
    allCommitsID.map((c) => {
      const changedFilesRaw = execSync(`git diff-tree --no-commit-id --name-only -r ${c}`, execOptions)
        .trim()
        .split("\n");
      changedFilesRaw.map((f) => changedFiles.add(f));
    });

    /**
     * Con tutti i file cambiati posso confrontarli con l'array allPackages che contiene tutti i pacchetti del repo,
     * se la location è contenuta all'interno del path del file allora il pacchetto è cambiato, es:
     * 1. itero allPackages
     * 2. iterazione 1: {"location":"packages/backoffice/app","name":"@monorepov3/backoffice-app"}
     * 3. se location quindi packages/backoffice/app è cotenuta in un file modificato allora vuol dire che è cambiato il pacchetto,
     *    e aggiungo {"location":"packages/backoffice/app","name":"@monorepov3/backoffice-app"} all'array dei pacchetti modificati
     */
    allPackages.map((p) => {
      for (const c of changedFiles) {
        if (c.includes(p.location)) {
          changed.push(p);
          break;
        }
      }
    });

    // Ora devo verificare se uno dei pacchetti cambiati è una dipendenza di un'altro pacchetto, se è così allora di conseguenza
    // va aggiunto anche quel pacchetto tra quelli cambiati perchè dovrà per forza di cose essere ribuildato
    allPackages.map((p) => {
      // Proseguo SOLO Se NON sto iterando un pacchetto che so che è già cambiato
      if (changed.filter((c) => c.name === p.name).length === 0) {
        const packageJSONPath = path.join(cwd, p.location, "package.json");
        const data = JSON.parse(fs.readFileSync(packageJSONPath));

        if (data.dependencies) {
          Object.keys(data.dependencies).map((d) => {
            if (changed.filter((c) => c.name === d).length !== 0) changed.push(p);
          });
        }
      }
    });

    return changed;
  }

  /**
   *
   * @param {*} _path
   * @param {*} _strategy
   * @param {*} _preRelease
   * @param {*} _preReleaseID
   * @returns nuova versione del package, viene incrementata in base ai parametri presenti in ingresso
   */
  bumpVersion(currentVersion = "", _strategy = STRATEGY_PATCH, _preRelease = false, _preReleaseID = "beta") {
    const defaultStrategies = [STRATEGY_MAJOR, STRATEGY_MINOR, STRATEGY_PATCH, STRATEGY_PRERELEASE, STRATEGY_PREMAJOR];

    if (!defaultStrategies.includes(_strategy)) {
      console.log(
        chalk.underline.red(`You provided not supported strategy. Choose one between ${defaultStrategies.join(", ")}`)
      );
      return;
    }

    const semver = require("semver");

    let newVersion = "";
    if (_preRelease === true) {
      newVersion = semver.inc(currentVersion, _strategy, _preReleaseID);
    } else {
      newVersion = semver.inc(currentVersion, _strategy);
    }

    console.log(
      `Version increased from ${chalk.bold(currentVersion)} to ${chalk.bold(newVersion)} ${emoji.get("rocket")}`
    );

    return newVersion;
  }

  commitChanges(msg) {
    const changes = execSync(`git status --porcelain`, execOptions);

    if (changes.trim() === "") return;

    const gitCheckout = execSync(`git checkout -b tmp`, execOptions);
    const gitAdd = execSync(`git add .`, execOptions);
    const gitCommit = execSync(`git commit -m "${msg}"`, execOptions);
    const gitpush = execSync(
      `git push "https://${this.user_key}:${this.access_token}@${this.repo_url}" "HEAD:${this.branch}" -o skip-ci`,
      execOptions
    );
    const gitCheckoutBack = execSync(`git checkout ${this.branch}`, execOptions);
    const gitCheckoutPull = execSync(`git pull`, execOptions);
    const gitDeleteBranch = execSync(`git branch -D tmp`, execOptions);
  }

  writeVersionForPackage(_path, version) {
    const packageJSONPath = path.join(cwd, _path, "package.json");
    const packageJSON = JSON.parse(fs.readFileSync(packageJSONPath));
    packageJSON.version = version;
    fs.writeFileSync(packageJSONPath, JSON.stringify(packageJSON, null, 2));
  }

  readVersionForPackage(_path) {
    const packageJSONPath = path.join(cwd, _path, "package.json");
    const packageJSON = JSON.parse(fs.readFileSync(packageJSONPath));
    return packageJSON.version;
  }

  prepareBranch() {
    // Resettiamo prima la versione a prescindere perchè potrebbe per qualche ragione essere stata presa in un momento di merge
    // di main di un'altro branch e siccome non abbiamo delle funzionalità premium di gitlab dobbiamo fare queste modifiche dopo che il codice ormai è stato
    // già cambiato e quindi facciamo qualche controllo in più.
    // Inoltre siccome è possibile creare un branch uguale a uno magari appena mergiato potrebbe capitare che la versione che vogliamo scrivere
    // di prerelease sia uguale a quella che magari era stata appena mergiata e ha lasciato il codice sporco, quindi facciamo prima un commit di reset se è necessario
    // e poi andiamo a scrivere la versione.

    // Controlliamo che la versione sia la 1.0.0
    if (this.readVersionForPackage("") !== "1.0.0") {
      // Andiamo a resettare la versione facendo anche un commit.
      this.writeVersionForPackage("", "1.0.0");
      this.commitChanges(`Reset to version 1.0.0`);
    }

    const newVersion = this.bumpVersion("1.0.0", STRATEGY_PRERELEASE, true, this.branch);
    this.writeVersionForPackage("", newVersion);
    this.commitChanges(`Initial commit ${this.branch}`);
  }

  /**
   *
   * @param {*} package
   * @param {*} tag
   * @returns
   */
  /**
   * 1. Con il comando sotto recuperiamo solo i package di quel pacchetto ordinati per numero di versione (da capire se funziona
   *    l'ordinamento anche con il prelrease, se non va utilizziamo semver per fare i check di versione).
   * 2. dobbiamo prendere SOLO il pacchetto con il tag che ci interessa (latest per la produzione)
   * 3. In teoria dovrebbe esistere solo 1 perchè il tag è unico quindi prendiamo (se non esiste è
   *    il caso di prima release per quel paccheto e tag e quindi andiamo a mettere la 1.0)
   */
  async readLatestVersionFromRegistry(_package, tag = "latest") {
    const { Gitlab } = require("@gitbeaker/node");
    const api = new Gitlab({
      token: "TaLdtL4bTfuYp8h1Pqu8",
    });

    const packagesRegistry = await api.Packages.all({
      projectId: this.project_id,
      package_name: _package.name,
      order_by: "version",
      sort: "desc",
    });

    if (packagesRegistry.length === 0) {
      // Non esistono relese
      return null;
    }

    let latestPackage = null;
    for (const p of packagesRegistry) {
      const latest = p.tags.find((t) => t.name === tag);
      if (latest !== undefined) {
        latestPackage = p;
        break;
      }
    }

    if (latestPackage !== null) {
      return latestPackage.version;
    }

    return null;
  }

  async buildPackages(packages) {
    const copyPacakges = [...packages];
    const scopeName = JSON.parse(fs.readFileSync(path.join(cwd, "package.json"))).name;

    /**
     * Siccome i vari packages possono avere dipendenze con gli altri packages del progetto, facciamo un ciclo while
     * e iteriamo uno a uno i package cambiati, se hanno dipendenze che devono ancora essere buildate (perchè sono all'interno
     * del while che stiamo iterando) allora proseguiamo altrimenti se sono tutte buildate risolviamo le loro versioni leggendo
     * dal registry se esistono tra il flusso corrente di development e quelle con tag latest
     */
    while (copyPacakges.length !== 0) {
      const builded = []; // Qui ci mettiamo i package buildate che poi toglieremo dall'array che iteriamo
      const arrPackagesName = copyPacakges.map((p) => p.name);

      for (const p of copyPacakges) {
        const packageJSONPath = path.join(cwd, p.location, "package.json");
        const packageJSON = JSON.parse(fs.readFileSync(packageJSONPath));
        let check = true;

        if (packageJSON.dependencies) {
          Object.keys(packageJSON.dependencies).map((d) => {
            if (arrPackagesName.includes(d)) {
              // Una dipendenza del pacchetto deve essre ancora buildata, quindi mettiamo il check a false così possiamo proseguire
              check = false;
            }
          });
        }

        // Se entriamo qui vuol dire che possiamo buildare il progetto, convertendo le eventuali dipendenze locali
        // con quelle che si trovano nel registry
        if (check === true) {
          if (packageJSON.dependencies) {
            Object.keys(packageJSON.dependencies).map(async (d) => {
              if (d.includes(scopeName)) {
                // Contiene una dipendenza del workspace, andiamo a recuperarla da registry.
                // Se siamo nel flusso di sviluppo allora controlliamo se esiste con il tag di sviluppo
                // altrimenti in qulsiasi altro caso si usa sempre la latest che dovrebbe SEMPRE esistere
                // perchè i pacchetti vengono risolti sempre da quello che non ha dipendenze (del workspace) o ha dipendenze risolte (del workspace) in poi
                let version = null;
                if (this.env === "development") {
                  version = await this.readLatestVersionFromRegistry(d, this.branch);
                }

                if (version === null) {
                  version = await this.readLatestVersionFromRegistry(d);
                }

                packageJSON.dependencies[d] = version;
              }
            });
          }

          // Andiamo ad incrementare la versione del pacchetto stesso che servirà poi poterla pushare nel registry,
          // come sempre andiamo prima a verificre se è presente nel registry sia di sviluppo corrente che se non c'è di produzione.
          let version = null;
          if (this.env === "development") {
            version = await this.readLatestVersionFromRegistry(p, this.branch);
          }

          if (version === null) {
            version = await this.readLatestVersionFromRegistry(p);
          }

          // Se non è presente neanche quella di produzione allora vuol dire che non è mai stato buildato ed è un progetto nuovo
          // gli mettiamo la versione 0.9 così incrementiamo a 1.0.0
          let strategy = STRATEGY_PATCH;
          if (version === null) {
            version = "0.9.0";
            if (this.env === "development") {
              strategy = STRATEGY_PREMAJOR;
            } else {
              strategy = STRATEGY_MAJOR;
            }
          } else {
            if (this.env === "development") {
              strategy = STRATEGY_PRERELEASE;
            }
          }

          if (this.env === "development") {
            packageJSON.version = this.bumpVersion(version, strategy, true, this.branch);
          } else {
            packageJSON.version = this.bumpVersion(version, strategy);
          }

          fs.writeFileSync(packageJSONPath, JSON.stringify(packageJSON, null, 2));

          // Dopo aver risolto eventuali dipendenze possiamo buildare il progetto
          this.buildPackage(p);

          // Possiamo pubblicare il progetto
          this.publishPackage(p, this.env === "development" ? this.branch : undefined);

          // Puliamo la working copy perchè avevamo modificato il package.json
          this.cleanWorkingCopy();

          // Aggiungiamo il pacchetto buildato in un oggetto temporaneo così da poter rimuoverlo dai pacchetti ancora da buildare
          builded.push(p);
        }
      }

      // Finita l'iterazione prima di passare al prossimo controllo del ciclo while andiamo a rimuovere
      // i pacchetti buildati dall'array di controllo del ciclo while
      builded.map((b) => {
        // Troviamo l'indice dell'array che vogliamo rimuovere
        const index = copyPacakges.findIndex((p) => p.name === b.name);
        // Rimuoviamo l'elemento
        copyPacakges.splice(index, 1);
      });
    }
  }

  buildPackage(_package) {
    const execBuild = execSync(`yarn workspace ${_package.name} build`, execOptions);
  }

  publishPackage(_package, tag = "latest") {
    const execPublish = execSync(`yarn workspace ${_package.name} npm publish --tag ${tag}`, execOptions);
  }

  cleanWorkingCopy() {
    const clean = execSync(`git checkout -- .`, execOptions);
  }

  devRelease() {
    const firstLastCommits = this.getFirstAndLastHashOnBranch();

    if (firstLastCommits.length === 0) {
      console.log(chalk.underline.red(`No packages changed.`));
      return;
    }

    const cleanedHash = this.filterCommitsBasedOnLastCommand("ci-release", firstLastCommits[0], firstLastCommits[1]);

    if (cleanedHash.length === 0) {
      console.log(chalk.underline.red(`No packages changed.`));
      return;
    }

    const changedPackages = this.getChangedPackages(cleanedHash[0], cleanedHash[1]);

    if (changedPackages.length === 0) {
      console.log(chalk.underline.red(`No packages changed.`));
      return;
    }

    this.buildPackages(changedPackages);
  }

  rebuildAllBranchHistory() {
    const firstLastCommits = this.getFirstAndLastHashOnBranch();

    if (firstLastCommits.length === 0) {
      console.log(chalk.underline.red(`No packages changed.`));
      return;
    }

    const changedPackages = this.getChangedPackages(firstLastCommits[0], firstLastCommits[1]);

    if (changedPackages.length === 0) {
      console.log(chalk.underline.red(`No packages changed.`));
      return;
    }

    this.buildPackages(changedPackages);
  }

  async getFromRegistryAllPackagesWithTag(tag = "latest") {
    const { Gitlab } = require("@gitbeaker/node");
    const api = new Gitlab({
      token: "TaLdtL4bTfuYp8h1Pqu8",
    });

    const _packagesTmp = [];
    const packagesRegistry = await api.Packages.all({
      projectId: this.project_id,
      order_by: "version",
      sort: "desc",
    });

    for (const p of packagesRegistry) {
      const latest = p.tags.find((t) => t.name === tag);
      if (latest !== undefined) {
        _packagesTmp.push(p);
      }
    }

    const packages = [];
    const allPackages = this.getAllPackages();
    _packagesTmp.map((packReg) => {
      const retrievedPackaged = allPackages.find((p) => p.name === packReg.name);
      packages.push(retrievedPackaged);
    });

    return packages;
  }

  async deleteFromRegistryWithSpecificPrereleaseVersion(version) {
    const { Gitlab } = require("@gitbeaker/node");
    const api = new Gitlab({
      token: "TaLdtL4bTfuYp8h1Pqu8",
    });

    const packages = [];
    const packagesRegistry = await api.Packages.all({
      projectId: this.project_id,
      order_by: "version",
      sort: "desc",
    });

    const filtered = packagesRegistry.filter((p) => p.version.includes(`-${version}.`));

    filtered.map(async (p) => {
      await api.Packages.remove(this.project_id, p.id);
    });
  }

  async prodRelease() {
    // Recuperiamo dal package.json della root il nome del branch per poter andare a legggere dal registry i suoi pacchetti
    const versionMerged = this.readVersionForPackage("");
    let branchMerged = versionMerged.match(/(?<=\d+\.\d+\.\d+-).*?(?=\.\d+)/g);

    if (branchMerged === null) {
      // Non sono stati modificati progetti, ed è un commit che non proviene da uno sviluppo sui packages quindi non dobbiamo buildare
      console.log(chalk.underline.green(`No tag prelease found. Nothing to build.`));
      return;
    } else {
      branchMerged = branchMerged[0];
    }

    /**
     * Andiamo a resettare il numero di versione di root perchè era stato incrementato con il prerelease per creare un nuovo branch.
     * Lo mettiamo sempre di default a 1.0.0
     */
    this.writeVersionForPackage("", "1.0.0");
    this.commitChanges(`Reset to version 1.0.0`);

    // Recuperiamo dal registry tutti i pacchetti che hanno come tag il nome del branch appena mergiato
    const packages = await this.getFromRegistryAllPackagesWithTag(branchMerged);

    if (packages.length === 0) {
      console.log(chalk.underline.red(`No packages found in the registry for the version tag ${branchMerged}`));
      return;
    }

    // Buildiamo i packages
    this.buildPackages(packages);

    // Eliminiamo dal registry tutti i pacchetti che hanno come versione la prerelease appena mergiata
    await this.deleteFromRegistryWithSpecificPrereleaseVersion(branchMerged);
  }

  async prodRebuildAllPackages() {
    // Recuperiamo dal package.json della root il nome del branch per poter andare a legggere dal registry i suoi pacchetti
    const currentVersion = this.readVersionForPackage("");

    if (currentVersion !== "1.0.0") {
      this.writeVersionForPackage("", "1.0.0");
      this.commitChanges(`Reset to version 1.0.0`);
    }

    const packages = this.getAllPackages();

    if (packages.length === 0) {
      console.log(chalk.underline.red(`No packages found in the registry for the version tag ${branchMerged}`));
      return;
    }

    // Buildiamo i packages
    this.buildPackages(packages);
  }
}

module.exports = CICore;
